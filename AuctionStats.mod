<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="AuctionStats" version="2.2.1" date="6/7/2009" >
	<VersionSettings gameVersion="1.3.0" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Author name="filcon, lefi, laconic" email="filcon@chello.cz" />
		<Description text="Addon for auction statistics and enhancements of Auction House Window. AuctionAssist Incorporated By Lefi [Hell-Fire Empire]." />
		
		<Dependencies>
			<Dependency name="EASystem_LayoutEditor"	optional="false"	forceEnable="true"	/>
			<Dependency name="EASystem_Utils"			optional="false"	forceEnable="true"	/>
			<Dependency name="EASystem_WindowUtils"		optional="false"	forceEnable="true"	/>
			<Dependency name="EA_AuctionHouseWindow"	optional="false"	forceEnable="true"	/>
			<Dependency name="EA_BackpackWindow"		optional="false"	forceEnable="true"	/>
			<Dependency name="LibSlash"					optional="false"	forceEnable="true"	/>
		</Dependencies>

		<Files>
			<File name="AuctionStats.lua" />
			<File name="Locale.lua" />
			<File name="Options.lua" />
			<File name="Tooltip.xml" />
			<File name="Options.xml" />
			<File name="Checkbox.xml" />
			<File name="AuctionAssist.lua" />
		</Files>
		
		<OnInitialize>
			<CreateWindow name="AuctionToolTip" show="false" />
			<CreateWindow name="AuStatsOptions" show="false" />
			<CallFunction name="AuctionStats.Initialize" />
			<CallFunction name="AuctionAssist.Init" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="AuctionStats.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="AuctionAssist.CleanUp" />
			<CallFunction name="AuctionStats.Shutdown" />
		</OnShutdown>			
		<SavedVariables>
			<SavedVariable name="AuctionDB" global="true" />
			<SavedVariable name="AuctionAssistSettings" global="true" />
		</SavedVariables>		
	</UiMod>
</ModuleFile>
