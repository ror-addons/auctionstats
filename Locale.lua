local function cursefilterfix() -- needed so I can SVN commit past the curse filter.
	return true
end
AUCTION_STATS_LOCALE={}

AUCTION_STATS_LOCALE["enUS"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.",
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.",
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.",
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.",
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.",
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.",
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.",
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.",
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.",
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.",
		HELP1 = "Valid option for /au:",
		HELP2 = "options - open Auction Stats options window",
		HELP3 = "update - Update Auction Stats database",
		HELP4 = "stop - Stop updating Auction Stats database",
		HELP5 = "delete - DELETE Auction Stats database",
		INITIALIZED = "Auction Stats initialized.",
		UPDATE_FINISHED = "Auction Stats has finished updating.",
		UPDATE_PROGRESS = "Auction Stats progress",
		UPDATE_STARTED = "Auction Stats has started updating.",
		UPDATE_STOPPED = "Auction Stats has stopped updating.",
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["frFR"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["deDE"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["itIT"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["jaJP"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["koKR"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["ruRU"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["zhCN"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["esES"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Las opciones v�lidas para /au:",
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.

AUCTION_STATS_LOCALE["zhTW"] = {
	MESSAGE = {
		DATABASE_DELETED = "Auction Stats DB was deleted.", -- Requires localization
		ERROR_ALREADY_UPDATING = "Auction Stats Error: Auction Stats is already updating.", -- Requires localization
		ERROR_AUTOPRICE_INSUFFICIENT_DATA = "Aucction Stats Error: Cannot autoprice, insufficient data (no vendoe price, no median, no current auctions.", -- Requires localization
		ERROR_NO_LIBSLASH = "Auction Stats Error: Addon LibSlash not found.  Slash commands are disabled.", -- Requires localization
		ERROR_SLASH_COMMAND_AU = "Auction Stats Error: There was a problem registering the slash command /au.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID1 = "is not a valid option for '/au'.", -- Requires localization
		ERROR_SLASH_COMMAND_INVALID2 = "See '/au' for a list of valid options.", -- Requires localization
		ERROR_SLASH_COMMAND_UC = "Auction Stats Error: There was a problem registering the slash command /uc.  Another addon may be using that slash command.", -- Requires localization
		ERROR_SLASH_COMMAND_UNDERCUT = "Auction Stats Error: There was a problem registering the slash command /undercut.  Another addon may be using that slash command.", -- Requires localization
		ERROR_WAS_NOT_UPDATING = "Auction Stats Error: Auction Stats was not updating.", -- Requires localization
		HELP1 = "Valid option for /au:", -- Requires localization
		HELP2 = "options - open Auction Stats options window", -- Requires localization
		HELP3 = "update - Update Auction Stats database", -- Requires localization
		HELP4 = "stop - Stop updating Auction Stats database", -- Requires localization
		HELP5 = "delete - DELETE Auction Stats database", -- Requires localization
		INITIALIZED = "Auction Stats initialized.", -- Requires localization
		UPDATE_FINISHED = "Auction Stats has finished updating.", -- Requires localization
		UPDATE_PROGRESS = "Auction Stats progress", -- Requires localization
		UPDATE_STARTED = "Auction Stats has started updating.", -- Requires localization
		UPDATE_STOPPED = "Auction Stats has stopped updating.", -- Requires localization
	},
}

				cursefilterfix() -- needed so I can SVN commit past the curse filter.