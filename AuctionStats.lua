-- If another version/copy of AuctionStats is already loaded, don't load this one.
if AuctionStats then return end
--------------
AuctionStats = {}
--------------

--------------
local function print(txt)
	EA_ChatWindow.Print(towstring(txt))
end
--------------

local currentLocale = "enUS"
local update = false
local itemLevel = 1
local rarity = SystemData.ItemRarity.COMMON
local MAX_LEVEL = 41
local MAX_ITEM_RARITY = SystemData.ItemRarity.ARTIFACT
local HIGHLIGHT_PERCENT = 25
local MINIMUM_NEEDED_FOR_MEDIAN = 5

local TIME_DELAY = 5 -- update process interval
local timeLeft = TIME_DELAY

local DB_VERSION = 3

local OldCreateItemTooltip, OldUpdateForItem, OldPopulateRow

function AuctionStats.Initialize()
	RegisterEventHandler("AuctionOnSearchResult", SystemData.Events.AUCTION_SEARCH_RESULT_RECEIVED, "AuctionStats.OnSearchResultReceived")
	OldCreateItemTooltip = Tooltips.CreateItemTooltip
	Tooltips.CreateItemTooltip = AuctionStats.CreateItemTooltip
	
	OldUpdateForItem = CreateAuctionWindow.UpdateForItem
	CreateAuctionWindow.UpdateForItem = AuctionStats.UpdateForItem
	
	OldPopulateRow = AuctionListWindow.PopulateRow
	AuctionListWindow.PopulateRow = AuctionStats.SearchPopulateRow

	if (LibSlash ~= nil) then
		if (not LibSlash.RegisterSlashCmd("au", AuctionStats.OnSlashCommand)) then
			print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_SLASH_COMMAND_AU)
		end
		if (not LibSlash.RegisterSlashCmd("uc", AuctionAssist.OnSlash)) then
			print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_SLASH_COMMAND_UC)
		end
		if (not LibSlash.RegisterSlashCmd("undercut", AuctionAssist.OnSlash)) then
			print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_SLASH_COMMAND_UNDERCUT)
		end
	else
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_NO_LIBSLASH)
	end
	
	InitDatabase()
	
	print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.INITIALIZED)
end

function InitDatabase()
	if (AuctionDB == nil or AuctionDB.version ~= DB_VERSION) then
		AuctionDB = {
				version = DB_VERSION,
				ExitOnComplete = false,
				Data = {}
		}
	end
end

function AuctionStats.Shutdown()
	UnregisterEventHandler( SystemData.Events.AUCTION_SEARCH_RESULT_RECEIVED, "AuctionStats.OnSearchResultReceived" )
	LibSlash.UnregisterSlashCmd( "au" )
	LibSlash.UnregisterSlashCmd( "uc" )
	LibSlash.UnregisterSlashCmd( "undercut" )
	Tooltips.CreateItemTooltip = OldCreateItemTooltip
	CreateAuctionWindow.UpdateForItem = OldUpdateForItem
	AuctionListWindow.PopulateRow = OldPopulateRow
end

function AuctionStats.CreateItemTooltip( itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken )
	--d(L"auction.CreateItemTooltip")
	
	OldCreateItemTooltip( itemData, mouseoverWindow, anchor, disableComparison, extraText, extraTextColor, ignoreBroken )					
	AuctionStats.AddExtraWindow ("AuctionToolTip", "ItemTooltip", itemData)
end

function AuctionStats.UpdateForItem( itemData )
	--d(L"AuctionStats.UpdateForItem")
	OldUpdateForItem(itemData)

	--RegisterEventHandler( SystemData.Events.AUCTION_SEARCH_RESULT_RECEIVED, "AuctionAssist.OnSearchResultsReceived" )
	AuctionAssist.SetShouldHandleResults()

	local queryData = AuctionListDataManager.CreteEmptyQuery() -- Mythic spelled create wrong, so I have to also, until they fix it.

	queryData.itemName = itemData.name

	AuctionListDataManager.SendAuctionSearch(queryData)

end


function AuctionStats.OnUpdate(elapsed)
	timeLeft = timeLeft - elapsed
	if timeLeft > 0 then
		return
	end
	timeLeft = TIME_DELAY

	if (update) then

		local queryData = AuctionListDataManager.CreteEmptyQuery() -- Mythic spelled create wrong, so I have to also, until they fix it.

		queryData.minItemLevel = itemLevel - 1
		queryData.maxItemLevel = itemLevel
		queryData.minRarity = rarity

		AuctionListDataManager.SendAuctionSearch(queryData)

		local progress = math.floor((((itemLevel -1) / 2 * MAX_ITEM_RARITY ) + rarity) / (MAX_ITEM_RARITY * ((MAX_LEVEL + 1) / 2) / 100))
		rarity = rarity + 1
		--print(towstring("Auction Stats progress: " .. progress .. "%"))
		AuctionStats.UpdateProgressLabel(towstring(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_PROGRESS .. ": " .. progress .. "%"))
		if (rarity > MAX_ITEM_RARITY) then
			rarity = SystemData.ItemRarity.COMMON
			itemLevel = itemLevel + 2
			if (itemLevel > MAX_LEVEL) then
				update = false
				--print(L"Auction Stats finished updating.")
				AuctionStats.UpdateProgressLabel(towstring(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_FINISHED))
				if (AuctionDB.ExitOnComplete == true) then
					BroadcastEvent(SystemData.Events.EXIT_GAME)
				end
			end
		end
	end
end


function AuctionStats.OnSearchResultReceived(searchResultsTable)
	-- do this first, so it updates faster.
	if(AuctionAssist.ShouldHandleResults() == true) then
		AuctionAssist.OnSearchResultsReceived(searchResultsTable)
	end

	--d(L"AuctionStats.OnSearchResultReceived")
	AuctionStats.Populate(searchResultsTable)
end


function AuctionStats.Populate(searchResultsData)
	--d(L"AuctionStats.Populate")

	if (nil == searchResultsData) then
		SendDebug(L"No search Results data found!")
	end

	for i,auctionData in ipairs(searchResultsData) do
		local itemData = auctionData.itemData
		if (nil == itemData) then
			ERROR(L"AuctionStats.Populate ERROR: Item Data not found")
			continue
		end
		-- Laconic -- very dirty way to block your auctions from being indexed
		-- currently only for buyout, will add bids if I get around to rewrite
		local shouldBlock = false
		local slot
		for slot = 1, 10 do
			if(auctionData.sellerName == GameData.Account.CharacterSlot[slot].Name) then
				shouldBlock = true
			end
		end
		if (shouldBlock == false) then
			AuctionStats.PopulateRow( auctionData, itemData )
		end
	end
end

local function SetAuctionData(id, auctionIDLowerNum, auctionData)
	--d(L"AuctionStats local SetAuctionData")
	if (AuctionDB.Data[id].AuctionData == nil) then
		AuctionDB.Data[id].AuctionData = {}
	end
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum] = {}
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].sellerName = auctionData.sellerName
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].currentBid = auctionData.currentBid
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].auctionIDHigherNum = auctionData.auctionIDHigherNum
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].buyOutPrice = auctionData.buyOutPrice	
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].highBidderName = auctionData.highBidderName
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].stackCount = auctionData.itemData.stackCount
	AuctionDB.Data[id].AuctionData[auctionIDLowerNum].buyOutPricePerOne = math.floor(auctionData.buyOutPrice / auctionData.itemData.stackCount)
	
	if (AuctionDB.Data[id].AuctionData.Keys == nil) then
	AuctionDB.Data[id].AuctionData.Keys = {}	
	end
	table.insert(AuctionDB.Data[id].AuctionData.Keys, auctionIDLowerNum)
end

function AuctionStats.PopulateRow( auctionData, itemData )

	local id = tonumber(itemData.uniqueID)
	local auctionIDLowerNum = auctionData.auctionIDLowerNum

	if (itemData.stackCount > 0) then
		if (AuctionDB.Data[id] == nil) then
			AuctionDB.Data[id] = {}
			AuctionDB.Data[id].ItemData = itemData
			SetAuctionData(id, auctionIDLowerNum, auctionData)
			AuctionDB.Data[id].seen = 1
			AuctionDB.Data[id].IndexItem = true
			AuctionDB.Data[id].ShowToolTip = true

			AuctionDB.Data[id].minBuyOutPrice = math.floor(auctionData.buyOutPrice / itemData.stackCount)
			AuctionDB.Data[id].maxBuyOutPrice = AuctionDB.Data[id].minBuyOutPrice

			AuctionDB.Data[id].minBidPrice = math.floor(auctionData.currentBid / itemData.stackCount)
			AuctionDB.Data[id].maxBidPrice = AuctionDB.Data[id].minBidPrice

			if (auctionData.buyOutPrice == 0) then
				AuctionDB.Data[id].buyOutCount = 0
			else
				AuctionDB.Data[id].buyOutCount = 1
			end

			if (auctionData.highBidderName == L"") then
				AuctionDB.Data[id].bidCount = 0
			else
				AuctionDB.Data[id].bidCount = 1
			end

			--d(L"Add item")
			--d(towstring(itemData.uniqueID))
		else
			if (AuctionDB.Data[id].IndexItem == false) then
				return
			end
			--d(L"Existing item")
			if (AuctionDB.Data[id].AuctionData[auctionIDLowerNum] == nil) then
				AuctionDB.Data[id].seen = AuctionDB.Data[id].seen + 1
				SetAuctionData(id, auctionIDLowerNum, auctionData)

				if (auctionData.highBidderName ~= L"") then
					AuctionDB.Data[id].bidCount = AuctionDB.Data[id].bidCount + 1
				end

				if (auctionData.buyOutPrice ~= 0) then
					AuctionDB.Data[id].buyOutCount = AuctionDB.Data[id].buyOutCount + 1
				end
			end

			local buyOutPricePerOne = math.floor(auctionData.buyOutPrice / itemData.stackCount)
			if (AuctionDB.Data[id].minBuyOutPrice > buyOutPricePerOne and
				auctionData.buyOutPrice ~= 0) then
				AuctionDB.Data[id].minBuyOutPrice = buyOutPricePerOne
			end
			if (AuctionDB.Data[id].maxBuyOutPrice < buyOutPricePerOne) then
				AuctionDB.Data[id].maxBuyOutPrice = buyOutPricePerOne
			end

			local bidPricePerOne = math.floor(auctionData.currentBid / itemData.stackCount)
			if (AuctionDB.Data[id].minBidPrice > bidPricePerOne) then
				AuctionDB.Data[id].minBidPrice = bidPricePerOne
			end
			if (AuctionDB.Data[id].maxBidPrice < bidPricePerOne) then
				AuctionDB.Data[id].maxBidPrice = bidPricePerOne
			end
		end
	end
end


local function GetBuyoutMedian(id)
	if(AuctionDB.Data[id] == nil or AuctionDB.Data[id].AuctionData == nil or AuctionDB.Data[id].AuctionData.Keys == nil) then
		return 0
	end
	
	local tempMedian = {}
	for i,auctionIDLowerNum  in ipairs(AuctionDB.Data[id].AuctionData.Keys) do
		--d(towstring(auctionIDLowerNum))
		local buyOut = AuctionDB.Data[id].AuctionData[auctionIDLowerNum].buyOutPricePerOne
		if ( buyOut > 0) then
		--d(towstring(buyOut))
		table.insert(tempMedian,buyOut)
		end
	end

	table.sort(tempMedian)
	--d(#tempMedian)
	local medianLow, medianHigh, median
	if (#tempMedian > MINIMUM_NEEDED_FOR_MEDIAN) then
		if (#tempMedian % 2 == 0) then
			medianLow = tempMedian[#tempMedian / 2]
			medianHigh = tempMedian[(#tempMedian / 2) + 1]
			median = math.floor((medianLow + medianHigh) / 2)
		else
			median = tempMedian[math.ceil(#tempMedian / 2)]
		end
	else
		median = 0
	end
	--d(towstring(median))
	return median
end

function AuctionStats.AddExtraWindow (windowName, windowToAnchorTo, itemData)

	local bidPct, buyOutPct, id, g, s, b, seenCnt, value, priceWindow
	local extraText = L"No data available."
	local medianText = L""
	local rowCount = 1
	local FONT_HEIGHT = 20
	local TOP_MARGIN = 10

	--- Auction Stats info
	local id = tonumber(itemData.uniqueID)
	local stackCount = itemData.stackCount  	
	if (AuctionDB ~= nil and AuctionDB.Data[id] ~= nil) then
		if (AuctionDB.Data[itemData.uniqueID].ShowToolTip == false) then
			return
		end
		seenCnt = AuctionDB.Data[id].seen
		if (seenCnt > 0) then
			extraText = towstring("Seen " .. AuctionDB.Data[id].seen .. " times at auction total \n")
			rowCount = rowCount + 1

			g, s, b = MoneyFrame.ConvertBrassToCurrency (AuctionDB.Data[id].minBuyOutPrice * stackCount)
			extraText = extraText .. g .. L"g " .. s .. L"s " .. b .. L"b" .. L" min/ "

			g, s, b = MoneyFrame.ConvertBrassToCurrency (AuctionDB.Data[id].maxBuyOutPrice * stackCount)
			extraText = extraText .. g .. L"g " .. s .. L"s " .. b .. L"b" .. L" max BO"
			rowCount = rowCount + 1

			bidPct = math.floor(AuctionDB.Data[id].bidCount / seenCnt * 100);
			buyOutPct = math.floor(AuctionDB.Data[id].buyOutCount / seenCnt * 100);
			extraText = extraText .. L"\n" .. towstring(bidPct .. "% have bids, " .. buyOutPct .. "% have BO")
			rowCount = rowCount + 1

			if (AuctionDB.Data[id].bidCount > 0) then
				g, s, b = MoneyFrame.ConvertBrassToCurrency (AuctionDB.Data[id].minBidPrice * stackCount)
				extraText = extraText .. L"\n" .. g .. L"g " .. s .. L"s " .. b .. L"b" .. L" min/ "
				g, s, b = MoneyFrame.ConvertBrassToCurrency (AuctionDB.Data[id].maxBidPrice * stackCount)
				extraText = extraText .. g .. L"g " .. s .. L"s " .. b .. L"b" .. L" max bid"
				rowCount = rowCount + 1
			end

			local median = GetBuyoutMedian(id) * stackCount
			if (median > 0) then
				g, s, b = MoneyFrame.ConvertBrassToCurrency (median)
				medianText = g .. L"g " .. s .. L"s " .. b .. L"b" .. L" median buyout\n"
				rowCount = rowCount + 1
			end
		end
	else
		rowCount = rowCount + 1
	end

	if (itemData ~= nil and itemData.sellPrice >= 0) then
		value = itemData.sellPrice * itemData.stackCount
		priceWindow = "AuctionToolTipSellPrice"
		MoneyFrame.FormatMoney (priceWindow.."Money", value, MoneyFrame.SHOW_EMPTY_WINDOWS);
		LabelSetText (priceWindow.."Info", L"Sell to vendor");
		rowCount = rowCount + 1
	end

	local index = 1
	while( Tooltips.curExtraWindows[index] ~= nil ) do
		index = index + 1
	end

	WindowSetShowing( windowName, true )
	Tooltips.curExtraWindows[index] = { name = windowName, data = extraText };

	-- Anchor the additional tooltip windows differently, depending on which side
	-- of the cursor the tooltip is placed
	local tooltipX, tooltipY        = WindowGetScreenPosition (Tooltips.curTooltipWindow);
	local mouseoverX, mouseoverY    = WindowGetScreenPosition (Tooltips.curMouseOverWindow);

	local wstrWindowName    = StringToWString (windowName);
	local wstrAnchorName    = StringToWString (windowToAnchorTo);

	WindowClearAnchors (windowName);
	local w, h = WindowGetDimensions( windowToAnchorTo )
	WindowSetDimensions(windowName, 300, rowCount * FONT_HEIGHT + TOP_MARGIN)
	local screenWidth, screenHeight = WindowGetDimensions ("Root");

	if (tooltipY > mouseoverY and screenHeight > tooltipY + h + (rowCount * FONT_HEIGHT) + TOP_MARGIN) then
		WindowAddAnchor (windowName, "top", windowToAnchorTo, "bottomleft", - ( w /2 ), 0);
	else
	WindowAddAnchor (windowName, "bottom", windowToAnchorTo, "topleft", - ( w /2 ), 0);
	end

	WindowSetAlpha( windowName, 1.0 )
	LabelSetText( "AuctionToolTipInfo", extraText)
	LabelSetText("AuctionToolTipMedian", medianText)

end

function AuctionStats.SearchPopulateRow( rowName, auctionData, itemData )
	--d(L"AuctionStats.SearchPopulateRow")
	OldPopulateRow(rowName, auctionData, itemData)

	if(AuctionListDataManager.GetCurrentTabNumber() == AuctionHouseWindow.TAB_BROWSE_ID or AuctionListDataManager.GetCurrentTabNumber() == AuctionHouseWindow.TAB_BIDS_ID)then
		local id = tonumber(itemData.uniqueID)
		local medianPrice = GetBuyoutMedian(id) * itemData.stackCount
		local minPrice = math.floor(medianPrice * (100 - HIGHLIGHT_PERCENT) / 100)
		local maxPrice = math.ceil(medianPrice * (100 + HIGHLIGHT_PERCENT) / 100)

		if (AuctionDB ~= nil and AuctionDB.Data[id] ~= nil and AuctionDB.Data[id].seen > MINIMUM_NEEDED_FOR_MEDIAN and auctionData.buyOutPrice > 0) then
			if (auctionData.buyOutPrice <= minPrice) then
				-- someone is selling cheap
				LabelSetTextColor (rowName.."Seller", 64, 255, 64)
			elseif (auctionData.buyOutPrice >= maxPrice) then
				-- someone is selling expensive
				LabelSetTextColor (rowName.."Seller", 255, 64, 64)
			else
				-- someone is selling around median price
				LabelSetTextColor (rowName.."Seller", 255, 192, 0)
			end
		else
			-- we haven't seen enough for a median price, or there is no buyOutPrice so just color it grey.
			LabelSetTextColor (rowName.."Seller", 128, 128, 128)
		end

		if (AuctionDB ~= nil and AuctionDB.Data[id] ~= nil and AuctionDB.Data[id].seen > MINIMUM_NEEDED_FOR_MEDIAN and auctionData.currentBid >= 0) then
			if (auctionData.currentBid <= minPrice / 2) then
				-- someone is buying cheap
				LabelSetTextColor (rowName.."Duration", 64, 255, 64)
			elseif (auctionData.currentBid >= maxPrice / 2) then
				-- someone is buying expensive
				LabelSetTextColor (rowName.."Duration", 255, 64, 64)
			else
				-- someone is buying around median price
				LabelSetTextColor (rowName.."Duration", 255, 192, 0)
			end
		else
			-- we haven't seen enough for a median price, so just color it grey.
			LabelSetTextColor (rowName.."Duration", 128, 128, 128)
		end
	end
end

function AuctionStats.StartUpdate()
	-- Check to see if options window is showing, if not we will display message to chat.
	if not WindowGetShowing(AuStatsOptions) then
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_STARTED)
	end
	AuctionStats.UpdateProgressLabel(towstring(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_STARTED))
	itemLevel = 1
	rarity = SystemData.ItemRarity.COMMON		
	update = true
end

function AuctionStats.StopUpdate()
	-- Check to see if options window is showing, if not we will display message to chat.
	if not WindowGetShowing(AuStatsOptions) then
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_STOPPED)
	end
	AuctionStats.UpdateProgressLabel(towstring(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.UPDATE_STOPPED))	
	update = false
end

function AuctionStats.OnSlashCommand(input)
local opt, val = input:match("([a-z0-9]+)[ ]?(.*)")
	
	if not(opt)then
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.HELP1)
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.HELP2)
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.HELP3)
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.HELP4)
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.HELP5)
	elseif opt == "delete" then
		AuctionDB = nil
		InitDatabase()
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.DATABASE_DELETED)
	elseif opt == "update" then
		AuctionStats.StartUpdate()
	elseif opt == "stop" then
		AuctionStats.StopUpdate()
	elseif opt == "options" then                        		
		WindowSetShowing("AuStatsOptions", true)
	else
		print("'" .. opt .. "' " .. AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_SLASH_COMMAND_INVALID1)
		print(AUCTION_STATS_LOCALE[currentLocale].MESSAGE.ERROR_SLASH_COMMAND_INVALID2)
	end
end
