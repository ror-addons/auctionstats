
local parentControls = "AuStatsOptionsBody"
local itemInventorySlot = 0
local slotItemData = nil

function SetCheckBox (name, status)
    ButtonSetCheckButtonFlag(parentControls..name, status)
    ButtonSetPressedFlag(parentControls..name, status)    
end

function AuctionStats.InitOptionsWindow()    
    LabelSetText (parentControls .. "ItemIndexLabel", L"Index item")    
    LabelSetText (parentControls .. "ItemShowToolTipLabel", L"Show ToolTip")
    LabelSetText ("AuStatsOptionsTitleText", L"Auction Stats Options")
    LabelSetText (parentControls .. "ExitOnCompleteLabel", L"Exit game on complete update")    
    ButtonSetText(parentControls.."StartUpdate", L"Start Update")
    ButtonSetText(parentControls.."StopUpdate", L"Stop Update")
    if (AuctionDB.ExitOnComplete ~= nil) then
        SetCheckBox("ExitOnComplete", AuctionDB.ExitOnComplete)
    else
        SetCheckBox("ExitOnComplete", false)
    end               
end

function AuctionStats.CloseOptionsWindow()
    WindowSetShowing("AuStatsOptions", false)
end

function AuctionStats.UpdateProgressLabel(text)
    LabelSetText (parentControls .. "ProgressLabel", text)
end

function AuctionStats.ToggleCheckBox()
    if (slotItemData ~= nil) then
        local checkBoxName = SystemData.ActiveWindow.name
        local Status = not ButtonGetCheckButtonFlag(checkBoxName)
        ButtonSetCheckButtonFlag(checkBoxName, Status)
        ButtonSetPressedFlag(checkBoxName, Status)     

        local id = tonumber(slotItemData.uniqueID)
        if (AuctionDB ~= nil and AuctionDB.Data[id] ~= nil) then
            if (checkBoxName == parentControls.."ItemIndexChk") then
                AuctionDB.Data[id].IndexItem = Status               
            end
            if (checkBoxName == parentControls.."ItemShowToolTip") then
                AuctionDB.Data[id].ShowToolTip = Status               
            end
        end
    end    
end

function AuctionStats.ExitOnComplete()
    local checkBoxName = SystemData.ActiveWindow.name
    local Status = not ButtonGetCheckButtonFlag(checkBoxName)
    ButtonSetCheckButtonFlag(checkBoxName, Status)
    ButtonSetPressedFlag(checkBoxName, Status)
    if (AuctionDB ~= nil) then
        AuctionDB.ExitOnComplete = Status
    end
end

function AuctionStats.OnMouseOverItem(...)  
    if ( slotItemData and slotItemData.id and slotItemData.id > 0 ) then
      Tooltips.CreateItemTooltip(slotItemData, SystemData.ActiveWindow.name, Tooltips.ANCHOR_WINDOW_RIGHT);
    end 
end

function AuctionStats.OnLButtonUpItem()
    if Cursor.IconOnCursor() then    
        if (Cursor.Data and (Cursor.Data.Source == Cursor.SOURCE_INVENTORY or
							Cursor.Data.Source == Cursor.SOURCE_EQUIPMENT or
							Cursor.Data.Source == Cursor.SOURCE_BANK or
							Cursor.Data.Source == Cursor.SOURCE_CRAFTING_ITEM or
							Cursor.Data.Source == Cursor.SOURCE_CURRENCY_ITEM
		)) then
            local itemData = DataUtils.GetItemData( Cursor.Data.Source, Cursor.Data.SourceSlot )                        
            if ( itemData ) then
				local inventorySlot = Cursor.Data.SourceSlot
                Cursor.Clear ()                
                AuctionStats.UpdateItemOptions(itemData, inventorySlot)
            end
        end                        
    end 
end

function AuctionStats.UpdateItemOptions(itemData, inventorySlot)
    local id = tonumber(itemData.uniqueID)
    local stackCount = itemData.stackCount
    if (AuctionDB ~= nil and AuctionDB.Data[id] ~= nil) then
        local texture, x, y = GetIconData( itemData.iconNum )
        DynamicImageSetTexture( parentControls.."ItemImageIcon", texture, x, y )
        MoneyFrame.FormatMoney(parentControls.."ItemStartPrice", AuctionDB.Data[id].minBuyOutPrice * stackCount, MoneyFrame.SHOW_EMPTY_WINDOWS);
        MoneyFrame.FormatMoney(parentControls.."ItemBuyoutPrice", AuctionDB.Data[id].maxBuyOutPrice * stackCount, MoneyFrame.SHOW_EMPTY_WINDOWS);
        itemInventorySlot = inventorySlot
        slotItemData = itemData 
        
        if (AuctionDB.Data[id].IndexItem ~= nil and AuctionDB.Data[id].IndexItem == true) then
            SetCheckBox("ItemIndexChk", true)
        else
            SetCheckBox("ItemIndexChk", false)
        end
        if (AuctionDB.Data[id].ShowToolTip ~= nil and AuctionDB.Data[id].ShowToolTip == true) then
            SetCheckBox("ItemShowToolTip", true)
        else
            SetCheckBox("ItemShowToolTip", false)
        end                    
    end
end

function AuctionStats.OnRButtonUpItem()
    itemInventorySlot = 0
    slotItemData = nil
    DynamicImageSetTexture(parentControls.."ItemImageIcon", "", 0, 0)    
    MoneyFrame.FormatMoney(parentControls.."ItemStartPrice", 0, MoneyFrame.SHOW_EMPTY_WINDOWS)
    MoneyFrame.FormatMoney(parentControls.."ItemBuyoutPrice", 0, MoneyFrame.SHOW_EMPTY_WINDOWS)
    MoneyFrame.FormatMoney(parentControls.."ItemDepositPrice", 0, MoneyFrame.HIDE_EMPTY_WINDOWS)
    SetCheckBox("ItemIndexChk", false)
    SetCheckBox("ItemShowToolTip", false)
end

