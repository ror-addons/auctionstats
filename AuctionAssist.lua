AuctionAssist = { 
	UndercutTypes = {
		  Percent = 0
		, Amount = 1
	}
	, DefaultAuctionAssistSettings = {
		  AutoUndercut = true
		, UndercutAmount = .05
		, UndercutType = 0
		, ShowSearchItemName = false
		, SetBuyOut = true
	}
}
local CheckBoxHandlers = { }

function AuctionAssist.Init() 
	if(not AuctionAssistSettings) then
		AuctionAssistSettings = AuctionAssist.DefaultAuctionAssistSettings
	end
	
	if(not AuctionAssistSettings.UndercutAmount) then
		AuctionAssistSettings.UndercutAmount = .5
		AuctionAssistSettings.UndercutType = 0
	end
	
	if(AuctionAssistSettings.ShowSearchItemName == nil) then
		AuctionAssistSettings.ShowSearchItemName = false
	end
	if(AuctionAssistSettings.SetBuyOut == nil) then
		AuctionAssistSettings.SetBuyOut = true
	end
	
	-- register the standard events we need to hook
	RegisterEventHandler(SystemData.Events.L_BUTTON_DOWN_PROCESSED, "AuctionAssist.AuctionHouseWindowCreateSearchItemName_LButtonDown")
	
	AuctionAssist.EA_Window_Backpack_EquipmentLButtonDown = EA_Window_Backpack.EquipmentLButtonDown
	EA_Window_Backpack.EquipmentLButtonDown = AuctionAssist.EA_Window_Backpack_EquipmentLButtonDown_Override
	
	--create the checkboxes on the AH window
	local baseName = "AuctionHouseCreateAuctionSetBuyOutCheckbox"
	CreateWindowFromTemplate(baseName,"AuctionStatsCheckboxTemplate","AuctionHouseWindowCreateAuction")
	WindowAddAnchor(baseName,"bottomleft","AuctionHouseWindowCreateAuction","topleft",5,-55)
	LabelSetText(baseName.."Label",L"Set Buy Out")
	ButtonSetPressedFlag(baseName.."Button", AuctionAssistSettings.SetBuyOut)
	CheckBoxHandlers[baseName] = AuctionAssist.SetBuyOutClicked
	
	local baseName = "AuctionHouseCreateAuctionAutoUndercutCheckbox"
	CreateWindowFromTemplate(baseName,"AuctionStatsCheckboxTemplate","AuctionHouseWindowCreateAuction")
	WindowAddAnchor(baseName,"bottomleft","AuctionHouseWindowCreateAuction","topleft",5,-31)
	LabelSetText(baseName.."Label",L"Auto Undercut ("..AuctionAssist.FormatUndercut()..L")")
	ButtonSetPressedFlag(baseName.."Button", AuctionAssistSettings.AutoUndercut)
	CheckBoxHandlers[baseName] = AuctionAssist.AutoUndercutClicked	
end

local handleNextResults = false

function AuctionAssist.ShouldHandleResults()
	
	if(handleNextResults == true) then
		handleNextResults = false
		return true
	else
		return false
	end
end

function AuctionAssist.SetShouldHandleResults()
	handleNextResults = true
end

function AuctionAssist.OnSlash(cmd)
	local amount, percent, len, last
	len = string.len(cmd)
	last = string.sub(cmd, len, len)
	if(last == "%") then
		amount = tonumber(string.sub(cmd, 1, len-1)) / 100.0
		percent = "%"
	elseif (last == "g" or last == "G") then
		amount = tonumber(string.sub(cmd, 1, len-1)) * 10000
	elseif (last == "s" or last == "S") then
		amount = tonumber(string.sub(cmd, 1, len-1)) * 100
	elseif (last == "b" or last == "B") then
		amount = tonumber(string.sub(cmd, 1, len-1))
	else
		amount = tonumber(cmd)
	end
	
	if(not amount) then
		return
	end
	
	AuctionAssistSettings.UndercutAmount = amount
	
	if(percent == "%") then
		AuctionAssistSettings.UndercutType = AuctionAssist.UndercutTypes.Percent
	else
		AuctionAssistSettings.UndercutType = AuctionAssist.UndercutTypes.Amount
	end
	
	LabelSetText("AuctionHouseCreateAuctionAutoUndercutCheckboxLabel",L"Auto Undercut ("..AuctionAssist.FormatUndercut()..L")")
end

function AuctionAssist.ResetDefaults()
	AuctionAssistSettings = AuctionAssist.DefaultAuctionAssistSettings
end

function AuctionAssist.CheckboxClicked()
	CheckBoxHandlers[SystemData.MouseOverWindow.name]()
end

function AuctionAssist.AutoUndercutClicked()
	local nv = not ButtonGetPressedFlag("AuctionHouseCreateAuctionAutoUndercutCheckboxButton")
	ButtonSetPressedFlag("AuctionHouseCreateAuctionAutoUndercutCheckboxButton",nv)
	
	AuctionAssistSettings.AutoUndercut = nv
end

function AuctionAssist.SetBuyOutClicked()
	local nv = not ButtonGetPressedFlag("AuctionHouseCreateAuctionSetBuyOutCheckboxButton")
	ButtonSetPressedFlag("AuctionHouseCreateAuctionSetBuyOutCheckboxButton",nv)
	
	AuctionAssistSettings.SetBuyOut = nv
end

function AuctionAssist.CleanUp()
	EA_Window_Backpack.EquipmentLButtonUp = AuctionAssist.EA_Window_Backpack_EquipmentLButtonUp
	
	UnregisterEventHandler( SystemData.Events.L_BUTTON_DOWN_PROCESSED,		  "AuctionAssist.AuctionHouseWindowCreateSearchItemName_LButtonDown")
end

function AuctionAssist.CalculateUndercutPrice( amount )
	if(not AuctionAssistSettings.AutoUndercut) then
		return amount
	end
	
	local undercut = AuctionAssistSettings.UndercutAmount
	if(AuctionAssistSettings.UndercutType == AuctionAssist.UndercutTypes.Percent) then
		undercut = amount * undercut
	end
	if ( amount <= undercut ) then
		return 1 -- sell for 1 brass
	else
		return math.floor(amount - undercut)
	end
end

function AuctionAssist.OnSearchResultsReceived( results )
	if(CreateAuctionWindow.itemInventorySlot.backpack <= 0 or CreateAuctionWindow.itemInventorySlot.slot <= 0) then  -- 1.3 update --
		return
	end
	
	local i,v
	
	local minBid = nil
	local minBuyout = nil
	   
	
	for i, v in ipairs(results) do
		-- Laconic -- very dirty way to block your auctions from being indexed
		-- currently only for buyout, will add bids if I get around to rewrite
		local shouldBlock = false
		for slot = 1, 10 do
			if(v.sellerName == GameData.Account.CharacterSlot[slot].Name) then
				shouldBlock = true
			end
		end
		if(shouldBlock == false) then -- exclude stuff you are the seller of
			if(v.currentBid ~= nil and v.currentBid > 0) then
				local currentBidOne = v.currentBid / v.itemData.stackCount
				if(not minBid or currentBidOne < minBid) then
					minBid = currentBidOne
				end
			end
			if(v.buyOutPrice ~= nil and v.buyOutPrice > 0) then
				local currentBuyoutOne = v.buyOutPrice / v.itemData.stackCount
				if(not minBuyout or currentBuyoutOne < minBuyout) then
					minBuyout = currentBuyoutOne
				end
			end
		end
	end
	
	local itemData = EA_BackpackUtilsMediator.GetItemsFromBackpack(CreateAuctionWindow.itemInventorySlot.backpack)[CreateAuctionWindow.itemInventorySlot.slot]
	local basePrice = itemData.sellPrice
	
	if(minBid ~= nil) then
		minBid = AuctionAssist.CalculateUndercutPrice(minBid)
		if(minBid < basePrice) then
			minBid = basePrice
		end 
		AuctionAssist.SetAuctionPrice("Bid", minBid * itemData.stackCount)
	end
	
	if (AuctionAssistSettings.SetBuyOut == false ) then
		AuctionAssist.SetAuctionPrice("BuyOut", 0)
	elseif(minBuyout ~= nil) then
		minBuyout = AuctionAssist.CalculateUndercutPrice(minBuyout)
		
		if(minBid and minBuyout < minBid) then
			minBuyout = minBid
		end
		AuctionAssist.SetAuctionPrice("BuyOut", minBuyout * itemData.stackCount)
	end
end

function AuctionAssist.FormatUndercut()
	local amount = AuctionAssistSettings.UndercutAmount
	if(AuctionAssistSettings.UndercutType == AuctionAssist.UndercutTypes.Amount) then
		local g = math.floor (amount / 10000)
		local s = math.floor ((amount - (g * 10000)) / 100)
		local b = math.mod (amount, 100)
		return towstring(g.."g"..s.."s"..b.."b")
	else
		return towstring(amount * 100)..L"%"
	end
end

function AuctionAssist.SetAuctionPrice(fieldSet, amount)
	local g = math.floor (amount / 10000)
	local s = math.floor ((amount - (g * 10000)) / 100)
	local b = math.mod (amount, 100)
	
	TextEditBoxSetText("AuctionHouseWindowCreateAuction"..fieldSet.."PriceGoldText", towstring(g))
	TextEditBoxSetText("AuctionHouseWindowCreateAuction"..fieldSet.."PriceSilverText", towstring(s))
	TextEditBoxSetText("AuctionHouseWindowCreateAuction"..fieldSet.."PriceBrassText", towstring(b))
end

function AuctionAssist.AuctionHouseWindowCreateSearchItemName_LButtonDown()
	if (
		   string.sub(SystemData.MouseOverWindow.name,1,string.len("AuctionHouseWindowCreateSearchItemName"))=="AuctionHouseWindowCreateSearchItemName" 
		or string.sub(SystemData.MouseOverWindow.name,1,string.len("AuctionHouseWindowCreateAuctionItem"   ))=="AuctionHouseWindowCreateAuctionItem"    ) 
	then
        if(Cursor.IconOnCursor()) then
			local dropType = ""
			if(string.sub(SystemData.MouseOverWindow.name,1,string.len("AuctionHouseWindowCreateSearchItemName"))=="AuctionHouseWindowCreateSearchItemName") then
				dropType = "search"
			else
				dropType = "create"
			end
			
			local itemData = DataUtils.GetItemData( Cursor.Data.Source, Cursor.Data.SourceSlot )
			if(itemData ~= nil) then
				if(dropType == "search") then
					AuctionAssist.SearchForItem(itemData)
				elseif(dropType == "create" and AuctionAssistSettings.AutoSearchOnAuctionCreate) then
					AuctionAssist.SearchForAuctionItem(itemData)
				end
				if(dropType == "search") then -- only clear the cursor if its the search, we want the auction one to work.
					Cursor.Clear()
				end
			end
		end
	end
end

function AuctionAssist.SearchForItem( itemData )
	
	if(AuctionAssistSettings.ShowSearchItemName) then
		TextEditBoxSetText("AuctionHouseWindowCreateSearchItemName", itemData.name)
	end
	
	local queryData = AuctionListDataManager.CreteEmptyQuery() -- Mythic spelled create wrong, so I have to also, until they fix it.
	queryData.itemName = itemData.name
	AuctionListDataManager.SendAuctionSearch(queryData)
end

function AuctionAssist.EA_Window_Backpack_EquipmentLButtonDown_Override( slot, flags, ... )
	local handled = false
	
	
	if(not Cursor.IconOnCursor() and WindowGetShowing("AuctionHouseWindow")) then
		local shiftPressed  = (flags == SystemData.ButtonFlags.ALT);
		local ctrlPressed = (flags == SystemData.ButtonFlags.CONTROL)
		
		
		local itemData = EA_Window_Backpack.GetItemsFromBackpack(EA_Window_Backpack.currentMode)[slot]
		if(itemData ~= nil) then
			if(shiftPressed) then
				AuctionAssist.SearchForItem( itemData )
				handled = true
			elseif(ctrlPressed) then
				local inventorySlot = {}
				inventorySlot.backpack = inventory
				inventorySlot.slot = slot
				AuctionAssist.PutUpForAuction( inventorySlot, itemData )
				handled = true
			end
		end
	end
	if(not handled) then
		AuctionAssist.EA_Window_Backpack_EquipmentLButtonDown( slot, flags, ... )
	end
end

function AuctionAssist.PutUpForAuction(inventorySlot, itemData) 
    -- acquiring lock on inventory slot number
    if ( itemData ) and 
       ( CreateAuctionWindow.ItemIsAuctionable( itemData ) ) and
       ( EA_Window_Backpack.RequestLockForSlot( inventorySlot.slot, inventorySlot.backpack, "AuctionHouseWindowCreateAuction" ) ) then
        
        -- Check to see if there was an item already in slot that we need to release
	local itemData = EA_BackpackUtilsMediator.GetItemsFromBackpack(CreateAuctionWindow.itemInventorySlot.backpack)[CreateAuctionWindow.itemInventorySlot.slot]
        if (CreateAuctionWindow.itemInventorySlot.backpack > 0 and CreateAuctionWindow.itemInventorySlot.slot > 0 and inventorySlot ~= CreateAuctionWindow.itemInventorySlot) then
            EA_Window_Backpack.ReleaseLockForSlot(CreateAuctionWindow.itemInventorySlot.slot, CreateAuctionWindow.itemInventorySlot.backpack, "AuctionHouseWindowCreateAuction")
            CreateAuctionWindow.Clear()
        end
        
        if(inventorySlot ~= CreateAuctionWindow.itemInventorySlot) then
			--only autosearch if it is enabled.			
			CreateAuctionWindow.UpdateForItem( itemData )
			CreateAuctionWindow.itemInventorySlot = inventorySlot
		end
    end
end
